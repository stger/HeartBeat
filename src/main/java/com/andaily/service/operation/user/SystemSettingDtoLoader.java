package com.andaily.service.operation.user;

import com.andaily.domain.dto.user.SystemSettingDto;
import com.andaily.domain.log.LogRepository;
import com.andaily.domain.shared.BeanProvider;
import com.andaily.domain.user.SystemSetting;
import com.andaily.domain.user.UserRepository;
import com.andaily.infrastructure.DateUtils;

/**
 * 2017/1/21
 *
 * @author Shengzhao Li
 */
public class SystemSettingDtoLoader {

    private transient UserRepository userRepository = BeanProvider.getBean(UserRepository.class);
    private transient LogRepository logRepository = BeanProvider.getBean(LogRepository.class);


    public SystemSettingDtoLoader() {
    }

    public SystemSettingDto load() {

        final SystemSetting systemSetting = userRepository.findSystemSetting();
        final SystemSettingDto settingDto = new SystemSettingDto(systemSetting);

        final long amount = logRepository.amountOfFrequencyMonitorLogsBeforeDate(DateUtils.now());
        settingDto.setMonitorLogAmount(amount);

        return settingDto;
    }
}
